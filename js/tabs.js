/**Some formating borrowed from http://inspirationalpixels.com/tutorials/creating-tabs-with-html-css-and-jquery#step-html**/
jQuery(document).ready(function() {
    jQuery('.tabs .tab-links a').on('click', function(e)  {
        var currentAttrValue = jQuery(this).attr('href');


        // Show/Hide Tabs
        //default
        //jQuery('.tabs ' + currentAttrValue).show().siblings().hide();

        //slide 1
        //jQuery('.tabs ' + currentAttrValue).siblings().slideUp(400);
        //jQuery('.tabs ' + currentAttrValue).delay(400).slideDown(400);

        //fade
        //jQuery('.tabs ' + currentAttrValue).fadeIn(400).siblings().hide();

        //slide 2
        jQuery('.tabs ' + currentAttrValue).slideDown(400).siblings().slideUp(400);
        /*********************** ************************************/

        // Change/remove current tab to active
        jQuery(this).parent('li').addClass('active').siblings().removeClass('active');

        e.preventDefault();
    });
});
